
function monad(bfn) {
    function bind(fn, err = null) {
      try {
        if (fn == null) {
          return bfn;
        } else if (bfn == null) {
          return monad(null);
        } else {
          return monad(fn(bfn));
        }
      } catch (e) {
        if (err) {
          console.error(e, err);
        }
        return monad(null);
      }
    }
    return bind;
  }


function filterTable(data){

    function testinput(re, str) {
        if (str.search(re) != -1) {
        return true
        } else {
        return false
        }
    }
    return  data.filter(function(i){
        return testinput("@academlo.com",i.email)
     }) 
}




function generateTable(data){



    let tbody = document.getElementById("structure")
    
    data.map(function(row){
        let rowi  = document.createElement("tr")
        tbody.appendChild(rowi)

        var order = ["name","age","email","gender","social"]
        row = {
            "col1": row["name"],
            "col2": row["email"],
            "col3": row["age"],
            "col4": row["gender"],
            "col5": row["social"]
        }

        Object.keys(row).map(function(col){
            if (col != "col5"){
                let colj  = document.createElement("td")
                colj.setAttribute("class","tg-0lax")
                colj.innerText = row[col]
                rowi.appendChild(colj)
            }
            else{ 
  
                let colj  = document.createElement("td")
                let ul1  = document.createElement("ul")
                let li1  = document.createElement("li")
                let li2  = document.createElement("li")
                colj.appendChild(ul1)
                ul1.appendChild(li1)
                ul1.appendChild(li2)
                li1.innerText = row[col][0]["url"]
                li2.innerText = row[col][1]["url"]
                rowi.appendChild(colj)
            }
        })
    })





    return 0
}


function insertData(path){
    $.getJSON(path, function(data){
        monad(data)/**/(filterTable)/**/(generateTable)/**/(null)
    });
}



insertData("./src/data/data.json")
